.. _module_import:

#############
Module import
#############

Cet élément est accessible via 
(:menuselection:`Administration & Paramétrage --> Options Avancées --> Module Import`).

Ce module permet l'intégration de données dans l'application.

Standard
========

Ces imports dit *standard* permettent d'importer via des fichiers csv des données dans l'application.


Spécifiques
===========

geojson
-------

L'objectif de cet import est de venir mettre à jour le champ géographique d'une table via un fichier geojson au format suivant ::

    {
        "type": "FeatureCollection",
        "name": "import-geojson-01",
        "crs": { 
            "type": "name",
            "properties": {
                "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
            }
        },
        "features": [
            {
                "type": "Feature",
                "properties": {
                    "id": 14,
                    "id_conc": 1
                },
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": [ [ [ [ 2.162269863532345, 43.932334982006239 ], [ 2.162260449410437, 43.932339379625539 ], [ 2.162275324305131, 43.932356010074727 ], [ 2.162284738428571, 43.932351612454198 ], [ 2.162269863532345, 43.932334982006239 ] ] ] ]
                }
            },
            {   
                "type": "Feature",
                "properties": {
                    "id": 22,
                    "id_conc": 2
                },
                "geometry": null
            },
            {
                "type": "Feature",
                "properties": {
                    "id": 33,
                    "id_conc": 3
                },
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": [ [ [ [ 2.162269863532345, 43.932334982006239 ], [ 2.162260449410437, 43.932339379625539 ], [ 2.162275324305131, 43.932356010074727 ], [ 2.162284738428571, 43.932351612454198 ], [ 2.162269863532345, 43.932334982006239 ] ] ] ]
                }
            }
        ]
    }

Cet exemple permet de mettre à jour le champ **pgeom** dess enregistrements de
la table **emplacement**, dont la clé primaire **emplacement** correspond à la
valeur présente dans la propriété **id_conc** dans le fichier geojson. Cet
import va mettre à jour :

- l'emplacement 1 en lui ajoutant une géométrie,
- l'emplacement 2 en lui vidant sa géométrie,
- l'emplacement 3 en lui modifiant sa géométrie.


Voici les paramètres possibles pour cet import :

.. list-table:: 
   :widths: 15 15 15 55
   :header-rows: 1
  
   * - table
     - clé primaire
     - champ géométrique
     - description

   * - emplacement
     - emplacement
     - geom
     - geometry(Point,2154)

   * - emplacement
     - emplacement
     - pgeom
     - geometry(MultiPolygon,2154)

   * - cimetiere
     - cimetiere
     - geom
     - geometry(MultiPolygon,2154)

   * - zone
     - zone
     - geom
     - geometry(MultiPolygon,2154)

   * - voie
     - voie
     - geom
     - geometry(MultiPolygon,2154)
