.. _parametres_generaux:

###################
Paramètres généraux
###################

Cet élément est accessible via 
(:menuselection:`Administration --> Paramètre`).

C'est le listing de tous les paramètres généraux utilisés à divers endroits de
l'applicatif.

.. image:: a_administration-parametres-listing.png

Voici le descriptif des paramètres :

.. list-table:: 
   :widths: 20 80
   :header-rows: 1
  
   * - paramètre
     - description

   * - .. _option_localisation:
       
       "option_localisation"
     - Système de géolocalisation. Les valeurs possibles sont :
     
       - plan,
       - sig_interne,

   * - .. _taille_cercueil:
       
       "taille_cercueil"
     - Taille d'un cercueil. Par défaut la valeur est égale à 5.

   * - .. _taille_urne:

       "taille_urne"
     - Taille d'une urne.

   * - .. _taille_urne_simple:

       "taille_urne_simple"
     - Taille d'une urne simple.

   * - .. _taille_urne_partage:

       "taille_urne_partage"
     - Taille d'une urne partagée.

   * - .. _taille_cercueil_simple:

       "taille_cercueil_simple"
     - Taille d'un cercueil simple.

   * - .. _taille_cercueil_partage:

       "taille_cercueil_partage"
     - Taille d'un cercueil partagé.

   * - .. _taille_cercueil_herm:

       "taille_cercueil_herm"
     - Taille d'un cercueil hermétique.

   * - .. _taille_reliquaire:

       "taille_reliquaire"
     - Taille d'un reliquaire.

   * - .. _taille_reliquaire_partage:

       "taille_reliquaire_partage"
     - Taille d'un reliquaire partagé.

   * - .. _taille_non_renseigne:

       "taille_non_renseigne"
     - Taille d'une enveloppe de défunt non renseigné.

   * - .. _taille_reduction:
   
       "taille_reduction"
     - Taille de la réduction.

   * - .. _temps_reduction:

       "temps_reduction"
     - Temps obligatoire avant la réduction.

   * - .. _duree_terraincommunal:

       "duree_terraincommunal"
     - Durée par défaut pour un terrain communal.

   * - .. _superficie_terraincommunal:

       "superficie_terraincommunal"
     - Superficie par défaut pour un terrain communal.


Il existe d'autres paramètres dont les noms significatifs permettent de savoir
de quoi il s'agit.
