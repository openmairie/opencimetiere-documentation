.. _recherche:

#########
Recherche
#########


La rubrique Recherche permet de rechercher des emplacements, des défunts ou
des autorisations selon différents critères. Ce sont ces différents outils de
recherche que nous allons décrire ici.

.. image:: a_recherche-menu-rubrik-recherche.png


.. _recherche_globale:

La recherche globale
####################

Cet élément est accessible via 
(:menuselection:`Recherche --> Recherche globale`) ainsi que directement
depuis le tableau de bord :ref:`widget_recherche_globale`.

La recherche globale a pour but de retrouver tous les enregistrements
d'openCimetière liés à un nom dans les tables de l application avec :

* possibilité d'étendre la recherche sur une partie de mot (recherche élagie
  cochée)
* possibilité de restreindre la recherche a un cimetière
* possibilité de restreindre la recherche a un champ en particulier

Les résultats sont affichés par type d'élément. Le tri sur les colonnes des
tableaux résultats permettent de trier les résultats. Il suffit de cliquer
sur un élément de tableau pour accéder à l'emplacement en question.

.. image:: a_recherche-resultats-de-recherche-globale.png


.. _concession_a_terme:

Concession à terme
##################

Cet élément est accessible via 
(:menuselection:`Recherche --> Concession à terme`) ainsi que directement
depuis le tableau de bord :ref:`widget_concession_a_terme`.

Ne sont affichées que les concessions temporaires dont l'année de la date de terme est inférieure
ou égale à celle en cours.

.. _recherche_contrat:

Recherche Contrat
#################

Cet élément est accessible via 
(:menuselection:`Recherche --> Recherche Contrat`).

Cet écran liste tous les contrats présents dans l'application.
Une recherche avancée est disponible.

.. image:: a_recherche-recherche_contrat-listing.png

A partir de ce listing il est possible d'ajouter des contrats. 
Le formulaire d'ajout est identique à celui se trouvant dans l'onglet 'contrat' d'un emplacement (cf :ref:`contrat`). 
La particulatité étant qu'un champ en autocomplete permet de rechercher l'emplacemement lié au contrat que l'on veut ajouter.

.. image:: a_recherche-recherche_contrat-formulaire-ajout.png

Si on consulte un contrat à partir de ce listing un champ affiche les informations de l'emplacement lié sous forme de lien cliquable.
Lorsqu'on clique sur ce lien on accède à la fiche de l'emplacement.

.. image:: a_recherche-recherche_contrat-formulaire-consult.png


.. _concession_libre:

Concession libre
################

Cet élément est accessible via 
(:menuselection:`Recherche --> Concession libre`).

Cet écran liste les emplacements de type concession qui sont notés comme libres.


.. _colombarium_libre:

Colombarium libre
#################

Cet élément est accessible via 
(:menuselection:`Recherche --> Colombarium libre`).

Cet écran liste les emplacements de type colombarium qui sont notés comme
libres.


.. _terraincommunal_libre:

Terrain Communal libre
######################

Cet élément est accessible via 
(:menuselection:`Recherche --> Terrain Communal libre`).

Cet écran liste les emplacements de type terrain communal qui sont notés comme
libres.