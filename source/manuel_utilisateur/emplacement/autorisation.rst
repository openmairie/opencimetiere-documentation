.. _autorisation:

############################
Les autorisations (Contacts)
############################


Il est proposé de décrire dans ce paragraphe la saisie des autorisations
dans l'onglet "Contacts" de l'emplacement.
Tous les contacts liés à l'emplacement sont listés dans cet onglet.


.. image:: a_emplacement-emplacement_concession-onglet-contact-listing.png


Il existe trois types de contact :

* Concessionnaire
* Ayant droit
* Autre

Saisir un contact
##################

Le formulaire est identique en mode ajout et modification.

.. image:: a_emplacement-emplacement_concession-onglet-contact-formulaire-ajout.png


Les informations à saisir sont :

- la nature du contact qui peut être concessionnaire, ayant-droit ou autre (obligatoire)
- le titre (:ref:`titre_de_civilite`)
- le nom
- le nom d'usage
- le prénom
- la date de naissance
- si ce contact est décédé
- la parenté
- l'adresse
- le code postal
- le numéro de téléphone
- le courriel

