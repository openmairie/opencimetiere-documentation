.. _contrat:

#############
Les contrats
#############

Il est proposé de décrire dans ce paragraphe la saisie de contrat dans l'onglet
"contrat" de l'emplacement.

On accède à cet élément depuis l'onglet "Contrat" de l'emplacement.
Tous les contrat liés à l'emplacement sont listés dans cet onglet.

.. image:: a_emplacement-emplacement_concession-onglet-contrat-listing.png

Saisir un contrat
-----------------

Il y a deux manière de saisir un contrat :

- Soit on passe par le formulaire d'ajout de l'onglet Contrat dans l'emplacement
- Soit on passe par le listing des contrats (voir :ref:`recherche_contrat`)

Le formulaire est identique en mode ajout et modification.

.. image:: a_emplacement-emplacement_concession-onglet-contrat-formulaire-ajout.png


Les informations à saisir sont :

- la date de demande (date à laquelle le contrat est enregistré)
- l'origine (Achat, Renouvellement ou Transformation)
- le terme (peut être à perpétuité ou temporaire)
- la durée (0 si le terme est perpétuité)
- date de vente (date à laquelle le contrat prend effet)
- date de terme (calculé si le contrat est temporaire, date de vente + duree en année)
- montant (dépend du paramétrage et modifiable manuellement)
- monnaie (Euros, Francs et Anciens Francs)
- valide (permet de marquer le contrat comme validé ou non)


Calcul automatique du montant
-----------------------------

Cette fonctionnalité utilise les tarifs (voir :ref:`tarif`).
Il est possible de calculer le montant du contrat en utilisant le bouton se situant à gauche du champ montant.

Si aucun tarif ne correspond aux critère saisie dans le formulaire du contrat un message s'affiche dans l'overlay :

.. image:: a_emplacement-emplacement_concession-onglet-contrat-overlay-no-tarif.png

Lorsqu'un tarif correspond aux données saisi dans le formulaire du contrat alors l'overlay affiche le montant associé : 

.. image:: a_emplacement-emplacement_concession-onglet-contrat-overlay-tarif.png

Lors d'une transformation d'emplacement l'overlay affiche les données utilisés pour calculer le nouveau montant :

.. image:: a_emplacement-emplacement_concession-onglet-contrat-overlay-tarif-transformation.png


Valider un contrat
------------------

Il est possible de marquer un contrat comme validé en utilisant l'action contextuelle "Valider" d'un contrat.

.. image::a_emplacement-emplacement_concession-onglet-contrat-action-valider.png

En cas d'erreur le champ valide est disponible dans le formulaire de modification.


