.. _archivage:

#########
Archivage
#########


Le menu :menuselection:`Archives` contient le listing de tous les éléments archivés dans l'application. Ces éléments sont classés en cinq types :

* :ref:`emplacement_archive`,
* :ref:`defunt_archive`,
* :ref:`contact_archive`,
* :ref:`contrat_archive`.
* :ref:`operation_archive`.

On accède à ce listing depuis le menu
(:menuselection:`Archives`).

.. image:: a_archives-menu-rubrik-archives.png

Les éléments des différents listings ne sont ni modifiables, ni supprimables.

.. _emplacement_archive:

Emplacement
===========

Cet élément est accessible via (:menuselection:`Archives --> Emplacement`).

Liste de tous les emplacements archivés.

.. _defunt_archive:

Défunt
======

Cet élément est accessible via (:menuselection:`Archives --> Défunt`).

Liste de tous les défunts exhumés dont l'emplacement a été archivé.

.. _contact_archive:

Contact
=======

Cet élément est accessible via (:menuselection:`Archives --> Contact`).

Liste de tous les contacts des emplacements archivés.

.. _contrat_archive:

Contrat
=======

Cet élément est accessible via (:menuselection:`Archives --> Contrat`).

Liste de tous les contrats des emplacements archivés.


.. _operation_archive:

Opération
=========

Cet élément est accessible via (:menuselection:`Archives --> Opération`).

Liste de toutes les opérations des emplacements archivés.


Traitement d'archivage
======================

Il existe quatre types d'archivage correspondant à quatre natures d'emplacements différentes.

* :ref:`fin_concession`,
* :ref:`fin_colombarium`,
* :ref:`fin_enfeu`,
* :ref:`fin_terraincommunal`.

On accède à ces traitements d'archivage depuis le menu
(:menuselection:`Archives`).

Seule la fin de concession sera détaillée car le traitement est similaire pour les trois autres natures d'emplacements listées.

.. _fin_concession:

Fin de concession
-----------------

Cet élément est accessible via (:menuselection:`Archives --> Fin De Concession`).

La fin de concession est disponible pour les concessions à terme. Elle permet d'archiver la concession choisie afin d'en récupérer la place.

Le traitement de fin de concession consiste à :

- terminer la concession

- transférer les défunts de la concession marqués comme **non exhumés** à l'ossuaire

- conserver la trace des défunts de la concession marqués comme **exhumés** dans la liste des défunts archivés

- transférer les courriers dans la liste des courriers archivés

- transférer les travaux dans la liste des travaux archivés

- transférer le dossier numerisé dans la liste des dossiers archivés

- transférer les contacts dans la liste des contacts archivés

- transférer les contrats dans la liste des contrats archivés

Ceci permet d'avoir une traçabilité des concessions.

Les opérations traitées sur cette concession sont supprimées lors du traitement d'archivage.

Attention : Les opérations en cours doivent être traitées.

Il est créé un emplacement vide reprenant tous les éléments de
localisation.

Le lancement du traitement se fait avec le formulaire suivant :

.. image:: a_archivage-fin_concession-listing.png

Il faut saisir ensuite l'ossuaire et la date.

.. image:: a_archivage-fin_concession-formulaire-validation.png


.. warning::
 
	Le traitement est irréversible.

.. _fin_colombarium:

Fin de colombarium
------------------

Cet élément est accessible via (:menuselection:`Archives --> Fin De Colombarium`).

Le traitement est identique à celui de la partie : :ref:`fin_concession`.

.. _fin_enfeu:

Fin d'enfeu
-----------

Cet élément est accessible via (:menuselection:`Archives --> Fin D'enfeu`).

Le traitement est identique à celui de la partie : :ref:`fin_concession`.

.. _fin_terraincommunal:

Fin de terrain communal
-----------------------

Cet élément est accessible via (:menuselection:`Archives --> Fin De Terrain Communal`).

Le traitement est identique à celui de la partie : :ref:`fin_concession`.
