.. migration


####################
Migration de version
####################

Dans data/pgsql, il est proposé les scripts de migration de base suivants :

*******************
Vers la version 3.x
*******************

- De la version 2.10 à la version 3.0.0 : v3.0.0.sql
- De la version 3.0.0-a4 à la version 3.0.0-a5 : v3.0.0-a5.sql
- De la version 3.0.0-a5 à la version 3.0.0-a6 : v3.0.0-a6.sql
- De la version 3.0.0-a6 à la version 3.0.0-a7 : v3.0.0-a7.sql
- De la version 3.0.0-a7 à la version 3.0.0-a8 : v3.0.0-a8.sql
- De la version 3.0.0-a8 à la version 3.0.0-a9 : v3.0.0-a9.sql
- De la version 3.0.0-a5 à la version 3.0.0 : v3.0.0-a5_vers_v3.0.0.sql
- De la version 3.0.0-a9 à la version 3.0.0 : v3.0.0-a9_vers_v3.0.0.sql


*******************
Vers la version 4.x
*******************

- De la version 3.0.0 à la version 4.0.0 : v4.0.0.sql
- De la version 4.0.0 à la version 4.1.0 : v4.1.0.sql
